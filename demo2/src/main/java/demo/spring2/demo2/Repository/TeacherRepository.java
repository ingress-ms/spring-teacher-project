package demo.spring2.demo2.Repository;

import demo.spring2.demo2.Entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {


}
