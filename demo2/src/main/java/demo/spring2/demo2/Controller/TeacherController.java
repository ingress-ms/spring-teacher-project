package demo.spring2.demo2.Controller;


import demo.spring2.demo2.Entity.Teacher;
import demo.spring2.demo2.Service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/teacher")
public class TeacherController {



    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    public List<Teacher> getAll(){
        return teacherService.getAll();
    }

    @GetMapping("/{id}")
    public Teacher getById(@PathVariable Integer id) {
       return teacherService.getById(id);
    }

    @PostMapping("/create")
    public void create(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
    }

    @PutMapping("{id}/update")
    public void update(@PathVariable Integer id, @RequestBody Teacher teacher ) {
        teacherService.update(id, teacher);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        teacherService.delete(id);
    }
}
