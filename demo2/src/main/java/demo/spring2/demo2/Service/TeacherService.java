package demo.spring2.demo2.Service;

import demo.spring2.demo2.Entity.Teacher;
import demo.spring2.demo2.Repository.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }


    public Teacher getById(Integer id) {
        return teacherRepository.findById(id).get();
    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public void update(Integer id, Teacher teacher) {
        if (teacherRepository.findById(id).isPresent()) {
            teacher.setId(id);
            teacherRepository.save(teacher);
        } else throw new RuntimeException("Bu id ile istifadeci tapilmadi");

    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }



}
